import java.util.Scanner;
public class Noten {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] note = new String[6];
		note[0] = "Sehr gut";
		note[1] = "Gut";
		note[2] = "Befriedigend";
		note[3] = "Ausreichend";
		note[4] = "Mangelhaft";
		note[5] = "Ungenügend";
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Schreiben Sie die Note: ");
		int tab = tastatur.nextInt();
		
		switch (tab) {
		case 1:
			System.out.println("1: " + note[0]);
			break;
		case 2:
			System.out.println("2: " + note[1]);
			break;
		case 3:
			System.out.println("3: " + note[2]);
			break;
		case 4:
			System.out.println("4: " + note[3]);
			break;
		case 5:
			System.out.println("5: " + note[4]);
			break;
		case 6:
			System.out.println("6: " + note[5]);
			break;
		default:
			System.out.println("Fehler!");
			break;
			
		}
		
	}

}
