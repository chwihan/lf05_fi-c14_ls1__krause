import java.util.Scanner;

public class AB1Aufgabe4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner eingabe = new Scanner(System.in);
		
		//Deklaration und Initialisierung
		double a;
		double b;
		double c;
		
		//Eingabe f�r Volumen und Ausgabe
	       System.out.print("Geben Sie einen Wert (l�nge der Seite) zur Volumenberechnung eines W�rfels ein: ");
	       a = eingabe.nextDouble();
	       System.out.println("Volumen: " + volumenWuerfel(a));
	       
	       System.out.print("Geben Sie 3 Werte (l�ngen der Seiten) zur Volumenberechnung eines Quaders ein: ");
	       a = eingabe.nextDouble();
	       b = eingabe.nextDouble();
	       c = eingabe.nextDouble();
	       System.out.println("Volumen: " + volumenQuader(a, b, c));

	       System.out.print("Geben Sie 2 Werte (L�nge/H�he) zur Volumenberechnung einer Pyramide ein: ");
	       a = eingabe.nextDouble();
	       b = eingabe.nextDouble();
	       System.out.println("Volumen: " + volumenPyramide(a, b));
	       
	       System.out.print("Geben Sie den Radius zur Volumenberechnung einer Kugel ein: ");
	       a = eingabe.nextDouble();
	       System.out.println("Volumen: " + volumenKugel(a));
	       
	       eingabe.close();
	}
//Berechnungen und in den eigenene Methden + Return an main Methode
	static double volumenWuerfel(double a) {
		double v = a * a * a;
		return v;
	}

	static double volumenQuader(double a, double b, double c) {
		double x = a * b * c;
		return x;
	}

	static double volumenPyramide(double a, double h) {
		double y = a * a * (h / 3);
		return y;
	}

	static double volumenKugel(double r) {
		double z = (4 / 3) * r * r * r * Math.PI;
		return z;
	}
}
