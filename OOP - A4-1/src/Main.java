
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh'ta", 2);
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2);
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni'Var", 5);
		
		Ladung ladung1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung ladung2 = new Ladung("Borg-Schrott", 5);
		Ladung ladung3 = new Ladung("Rote Materie", 2);
		Ladung ladung4 = new Ladung("Forschungssonde", 35);
		Ladung ladung5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung ladung6 = new Ladung("Plasma-Waffe", 50);
		Ladung ladung7 = new Ladung("Photonentorpedo", 3);
		
		klingonen.addLadung(ladung1);
		klingonen.addLadung(ladung5);
		romulaner.addLadung(ladung2);
		romulaner.addLadung(ladung3);
		romulaner.addLadung(ladung6);
		vulkanier.addLadung(ladung4);
		vulkanier.addLadung(ladung7);
		
		System.out.println("-----------------------------------------");
		System.out.println(klingonen);
		System.out.println(ladung1);
		System.out.println(ladung5);
		System.out.println("-----------------------------------------");
		System.out.println(romulaner);
		System.out.println(ladung2);
		System.out.println(ladung3);
		System.out.println(ladung6);
		System.out.println("-----------------------------------------");
		System.out.println(vulkanier);
		System.out.println(ladung4);
		System.out.println(ladung7);
		System.out.println("-----------------------------------------");
		
		//klingonen.photonentorpedo();
		klingonen.treffer(klingonen.photonentorpedo(), klingonen.phaserkanone());
		System.out.println("\n");
		System.out.println("-----------------------------------------");
		//romulaner.photonentorpedo();
		romulaner.treffer(romulaner.photonentorpedo(),romulaner.phaserkanone());
		System.out.println("\n");
		System.out.println("-----------------------------------------");
		//vulkanier.photonentorpedo();
		vulkanier.treffer(vulkanier.photonentorpedo(), vulkanier.phaserkanone());
		System.out.println("-----------------------------------------");
		
	}

	
	
}
