public class Konfiguration_Chaos {

public static void main(String[] args) {
	//name
	String name;
	String typ = "Automat AVR";
	String bezeichnung = "Q2021_FAB_A";
	name = typ + " " + bezeichnung;
	
	System.out.println("Name: " + name);
	
	//sprache
	char sprachModul = 'd';
	System.out.println("Sprache: " + sprachModul);
	
	//Pr�fnummer
	
	final byte PRUEFNR = 4;
	System.out.println("Pr�fnummer : " + PRUEFNR);
	
	//f�llstand
	double fuellstand;
	double maximum = 100.00;
	double patrone = 46.24;
	fuellstand = maximum - patrone;
	int summe;
	System.out.println("F�llstand Patrone: " + fuellstand + " %");
	
	//euro
	int muenzenEuro = 130;
	int muenzenCent = 1280;
	summe = muenzenCent + muenzenEuro * 100;

	int euro;
	
	euro = summe / 100;
	System.out.println("Summe Euro: " + euro +  " Euro");
	
	//cent
	int cent;
	
	cent = summe % 100;
	System.out.println("Summe Rest: " + cent +  " Cent");
	
	
	//status
	
	boolean statusCheck;
	statusCheck = (euro <= 150)
	&& (sprachModul == 'd')
	&& (!(PRUEFNR == 5 || PRUEFNR == 6))
	&& (fuellstand >= 50.00)
	&& (euro >= 50)
	&& (cent != 0);
	System.out.println("Status: " + statusCheck); 
}
}