﻿import java.util.Scanner;

class Fahrkartenautomat
{

public static void main(String[] args)
{
	while (true) { //endlosschleife
	double zuZahlenderBetrag;
	double eingezahlterGesamtbetrag;

	zuZahlenderBetrag = fahrkartenbestellungErfassen();
	eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	fahrkartenAusgeben();
	zuZahlenderBetrag = rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
	
	}
}

public static double fahrkartenbestellungErfassen()
	{
	double zuZahlenderBetrag = 0;
	byte anzahl_tickets;
	//double einzelAB = 2.90;
	//double tagesAB = 8.60;
	//double kleinAB = 23.50;
	double[] preise = new double [10];
	preise[0] = 2.90;
	preise[1] = 3.30;
	preise[2] = 3.60;
	preise[3] = 1.90;
	preise[4] = 8.60;
	preise[5] = 9.00;
	preise[6] = 9.60;
	preise[7] = 23.50;
	preise[8] = 24.30;
	preise[9] = 24.90;
	
	
	String[] bezeichnung = new String[11];
	bezeichnung[0] = "Einzelfahrschein Regeltarif AB";
	bezeichnung[1] = "Einzelfahrschein Berlin BC";
	bezeichnung[2] = "Einzelfahrschein Berlin ABC";
	bezeichnung[3] = "Kurzstrecke";
	bezeichnung[4] = "Tageskarte Berlin AB";
	bezeichnung[5] = "Tageskarte Berlin BC";
	bezeichnung[6] = "Tageskarte Berlin ABC";
	bezeichnung[7] = "Kleingruppen-Tageskarte Berlin AB";
	bezeichnung[8] = "Kleingruppen-Tageskarte Berlin BC";
	bezeichnung[9] = "Kleingruppen-Tageskarte Berlin ABC";
	bezeichnung[10] = "Bezahlen";
	
	//Der Vorteil ist das man den Inhalt des Arrays bearbeiten kann und der
		// Text und Preis sich automatisch anpasst
	
	Scanner tastatur = new Scanner(System.in);
	
	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
	System.out.println(bezeichnung[0] + " [" + preise[0] + "0 EUR] (1)");
	System.out.println(bezeichnung[1] + " [" + preise[1] + "0 EUR] (2)");
	System.out.println(bezeichnung[2] + " [" + preise[2] + "0 EUR] (3)");
	System.out.println(bezeichnung[3] + " [" + preise[3] + "0 EUR] (4)");
	System.out.println(bezeichnung[4] + " [" + preise[4] + "0 EUR] (5)");
	System.out.println(bezeichnung[5] + " [" + preise[5] + "0 EUR] (6)");
	System.out.println(bezeichnung[6] + " [" + preise[6] + "0 EUR] (7)");
	System.out.println(bezeichnung[7] + " [" + preise[7] + "0 EUR] (8)");
	System.out.println(bezeichnung[8] + " [" + preise[8] + "0 EUR] (9)");
	System.out.println(bezeichnung[9] + " [" + preise[9] + "0 EUR] (10)");
	
	//Im Vergleich zum Anfang wo man den Text ausgeschrieben hat, vereinfacht das nutzen von Arrays
	//die Abfrage und Rechnung um ein vielfaches. Man muss den Text nicht hinschreiben und die Preise in doubles
	//Speichern, sondern kann beides in einen Array einfügen und dann jederzeit per Aufruf des Index darauf
	//zugreifen. Dadurch kann man den Preis und die Bezeichnung auch direkt im Array anpassen und es wird
	//übernommen und damit ausgeführt. Um ein weiteres Ticket hinzuzufügen muss man den Array einfach um eins
	//erweitern und den Preis und die Bezeichung angeben und die auswahl hinzufügen.
	
	int tab = tastatur.nextInt();
	do {
	
	if (tab == 1) {
		System.out.printf("Anzahl der Tickets: ");
			anzahl_tickets = tastatur.nextByte();
			
		if (anzahl_tickets <= 10) {
			System.out.printf("Zu zahlender Betrag (EURO): ");
	 	zuZahlenderBetrag = preise[0] * anzahl_tickets;
		}
		else if (anzahl_tickets > 10) {
			System.out.println("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
		}
	}
	else if (tab == 2) {
		System.out.printf("Anzahl der Tickets: ");
			anzahl_tickets = tastatur.nextByte();
			
		if (anzahl_tickets <= 10) {
			System.out.printf("Zu zahlender Betrag (EURO): ");
	 	zuZahlenderBetrag = preise[1] * anzahl_tickets;
		}
		else if (anzahl_tickets > 10) {
			System.out.println("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
		}
	}
	else if (tab == 3) {
		System.out.printf("Anzahl der Tickets: ");
		anzahl_tickets = tastatur.nextByte();
		
		if (anzahl_tickets <= 10) {
		System.out.printf("Zu zahlender Betrag (EURO): ");
		zuZahlenderBetrag = preise[2] * anzahl_tickets;
	}
		else if (anzahl_tickets > 10) {
			System.out.println("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
		}
	}
	else if (tab == 4) {
		System.out.printf("Anzahl der Tickets: ");
		anzahl_tickets = tastatur.nextByte();
		
		if (anzahl_tickets <= 10) {
			System.out.printf("Zu zahlender Betrag (EURO): ");
			zuZahlenderBetrag = preise[3] * anzahl_tickets;
		}
		else if (anzahl_tickets > 10) {
			System.out.println("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			}
		}
	else if (tab == 5) {
		System.out.printf("Anzahl der Tickets: ");
		anzahl_tickets = tastatur.nextByte();
		
		if (anzahl_tickets <= 10) {
			System.out.printf("Zu zahlender Betrag (EURO): ");
			zuZahlenderBetrag = preise[4] * anzahl_tickets;
		}
		else if (anzahl_tickets > 10) {
			System.out.println("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			}
		}
	else if (tab == 6) {
		System.out.printf("Anzahl der Tickets: ");
		anzahl_tickets = tastatur.nextByte();
		
		if (anzahl_tickets <= 10) {
			System.out.printf("Zu zahlender Betrag (EURO): ");
			zuZahlenderBetrag = preise[5] * anzahl_tickets;
		}
		else if (anzahl_tickets > 10) {
			System.out.println("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			}
		}
	else if (tab == 7) {
		System.out.printf("Anzahl der Tickets: ");
		anzahl_tickets = tastatur.nextByte();
		
		if (anzahl_tickets <= 10) {
			System.out.printf("Zu zahlender Betrag (EURO): ");
			zuZahlenderBetrag = preise[6] * anzahl_tickets;
		}
		else if (anzahl_tickets > 10) {
			System.out.println("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			}
		}
	else if (tab == 8) {
		System.out.printf("Anzahl der Tickets: ");
		anzahl_tickets = tastatur.nextByte();
		
		if (anzahl_tickets <= 10) {
			System.out.printf("Zu zahlender Betrag (EURO): ");
			zuZahlenderBetrag = preise[7] * anzahl_tickets;
		}
		else if (anzahl_tickets > 10) {
			System.out.println("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			}
		}
	else if (tab == 9) {
		System.out.printf("Anzahl der Tickets: ");
		anzahl_tickets = tastatur.nextByte();
		
		if (anzahl_tickets <= 10) {
			System.out.printf("Zu zahlender Betrag (EURO): ");
			zuZahlenderBetrag = preise[8] * anzahl_tickets;
		}
		else if (anzahl_tickets > 10) {
			System.out.println("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			}
		}
	else if (tab == 10) {
		System.out.printf("Anzahl der Tickets: ");
		anzahl_tickets = tastatur.nextByte();
		
		if (anzahl_tickets <= 10) {
			System.out.printf("Zu zahlender Betrag (EURO): ");
			zuZahlenderBetrag = preise[8] * anzahl_tickets;
		}
		else if (anzahl_tickets > 10) {
			System.out.println("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			}
		}
	}
	while (zuZahlenderBetrag <= 1);
	return zuZahlenderBetrag;
	
}

public static double fahrkartenBezahlen(double zuZahlenderBetrag)
	{
	Scanner tastatur = new Scanner(System.in);	
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		eingezahlterGesamtbetrag = 0.0;
       		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag) + "0");
    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
       }
		return eingezahlterGesamtbetrag;
	}

public static void fahrkartenAusgeben()
	{
		 System.out.println("\nFahrschein wird ausgegeben");
       		
       warte(250);
       System.out.println("\n\n");
	}

public static double rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag)
	{
		double rückgabebetrag;
       
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       muenzeAusgeben(rückgabebetrag);

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir wünschen Ihnen eine gute Fahrt.");
		return rückgabebetrag;
	}

	public static void warte(int zeit) {
		for (int i = 0; i < 8; i++){
			System.out.print("=");
		}
	    try {
	    	Thread.sleep(zeit);
		} catch (InterruptedException e) {
				e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(double rückgabebetrag) {
		 if(rückgabebetrag > 0.0)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
	}
}