import java.util.Scanner;

public class AB2Aufgabe2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Benutzereingaben lesen
		String artikel = liesString("was moechten Sie bestellen?"); //Auf die Methode verweisen/aufrufen
		//System.out.println("was moechten Sie bestellen?");
		//String artikel = myScanner.next();
		
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		//System.out.println("Geben Sie die Anzahl ein:");
		//int anzahl = myScanner.nextInt();
		
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		//System.out.println("Geben Sie den Nettopreis ein:");
		//double preis = myScanner.nextDouble();
		
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		//System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		//double mwst = myScanner.nextDouble();

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		//double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = berechneGesamtnettopreis(nettogesamtpreis, mwst);
		//double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
		//System.out.println("\tRechnung");
		//System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		//System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
	
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in); //Jede Methode brauch einen Scanner
		System.out.println(text); //Ausgabe des Textes aus der Main
		String artikel = myScanner.next(); //Eingabe
		return artikel; //Zur�ck zur Main
	}
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double preis = myScanner.nextDouble();
		return preis;
	}
	 
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl*nettopreis;
	}
	public static double berechneGesamtnettopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis*mwst;
	}
	 
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}
