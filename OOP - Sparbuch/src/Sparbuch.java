public class Sparbuch {
	
	private int kontonummer;
	private int kapital;
	private int zinssatz;
	int einzahlen;
	int auszahlen;
	int jahr;
	
	public Sparbuch() {
		this.kontonummer = 12345678;
		this.kapital = 1000;
		this.zinssatz = 2;
	}
	
	public Sparbuch(int ko, int ka, int z) {
		this.kontonummer = ko;
		this.kapital = ka;
		this.zinssatz = z;
	}
	
	public int zahleEin(int e) {
		einzahlen = e;
		this.kapital = this.kapital + einzahlen;
		return this.kapital;
	}
	
	public int hebeAb(int a) {
		auszahlen = a;
		this.kapital = this.kapital - auszahlen;
		return this.kapital;
	}
	
	public int getErtrag(int j) {
		jahr = j;
		int zinsen = this.kapital * (1 + (this.zinssatz / 100)) * j;
		this.kapital = zinsen + this.kapital;
		return this.kapital;
	}
	
	public double verzinse() {
		int zinsenjahr = (this.kapital * this.zinssatz)/100;
		this.kapital = zinsenjahr + this.kapital;
		return this.kapital;
	}
	
	public int getKontonummer() {
		return this.kontonummer;
	}
	
	public int getKapital() {
		return this.kapital;
	}
	
	public double getZinssatz() {
		return this.zinssatz;
	}
}
