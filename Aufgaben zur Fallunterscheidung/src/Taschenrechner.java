import java.util.Scanner;
public class Taschenrechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);
		double ergebnis;
		System.out.println("Geben Sie zwei Zahlen ein. ");
		
		System.out.println("erste Zahl: ");
		double zahl1 = tastatur.nextDouble();
		
		System.out.println("zweite Zahl: ");
		double zahl2 = tastatur.nextDouble();
		
		System.out.println("Was m�chten Sie rechnen? ");
		System.out.println("1. Addition (+), 2. Subtraktion (-), 3. Multiplikation (*), 4. Division (/): ");
		int tab = tastatur.nextInt();
		
		if (tab == 1) {
			ergebnis = zahl1 + zahl2;
			System.out.println("Das Ergebnis ist: " + ergebnis);
		}
		else if (tab == 2) {
			ergebnis = zahl1 - zahl2;
			System.out.println("Das Ergebnis ist: " + ergebnis);
		}
		else if (tab == 3) {
			ergebnis = zahl1 * zahl2;
			System.out.println("Das Ergebnis ist: " + ergebnis);
		}
		else if (tab == 4) {
			ergebnis = zahl1 / zahl2;
			System.out.println("Das Ergebnis ist: " + ergebnis);
		}
		else {
			System.out.println("Fehler!");
		}
	}

}
