import java.util.Scanner;
public class Aufgabe1_EigeneBedingungen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//Wenn beide Zahlen gleich sind, soll eine Meldung ausgegeben werden (If) 
//3. Wenn die 2. Zahl gr��er als die 1. Zahl ist, soll eine Meldung ausgegeben werden (If) 
//4. Wenn die 1. Zahl gr��er oder gleich als die 2. Zahl ist, soll eine Meldung ausgegeben werden, ansonsten eine andere Meldung (If-Else)
		
		//Deklaration der Variablen
		int ersteZahl;
		int zweiteZahl;
		
		//Eingabe des Scanners
		Scanner eingabe = new Scanner(System.in);
		System.out.print("Geben Sie zwei Zahlen ein: ");
		ersteZahl = eingabe.nextInt();
		zweiteZahl = eingabe.nextInt();
		
		if (ersteZahl == zweiteZahl)
		{
			System.out.println("Die Zahlen sind gleich!");
		}
		else if (zweiteZahl > ersteZahl)
		{
			System.out.println("Die zweite Zahl ist gr��er als die erste Zahl!");
		}
		else if (ersteZahl >= zweiteZahl)
		{
			System.out.println("Die erste Zahl ist gr��er oder gleich der zweiten Zahl");
		}
		else
		{
			System.out.println("Fehler!");
		}
	}

}
