
public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Kiste kiste1 = new Kiste();
		System.out.println("Die Farbe der Kiste ist:" + kiste1.getFarbe());
		System.out.println("Das Volumen betr�gt:" + kiste1.getVolumen());
		
		Kiste kiste2 = new Kiste(6, 6, 6, "blau");
		System.out.println("Die Farbe der Kiste ist:" + kiste2.getFarbe());
		System.out.println("Das Volumen betr�gt:" + kiste2.getVolumen());
		
		Kiste kiste3 = new Kiste(7, 7, 7, "rot");
		System.out.println("Die Farbe der Kiste ist:" + kiste3.getFarbe());
		System.out.println("Das Volumen betr�gt:" + kiste3.getVolumen());
	}

}
