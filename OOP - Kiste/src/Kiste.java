
public class Kiste {
	public int hoehe;
	public int breite;
	public int tiefe;
	public String farbe;
	
	public Kiste() {
		this.hoehe = 5;
		this.breite = 5;
		this.tiefe = 5;
		this.farbe = "braun";
	}
	
	public Kiste(int h, int b, int t, String f) {
		this.hoehe = h;
		this.breite = b;
		this.tiefe = t;
		this.farbe = f;
	}
	
	public int getVolumen() {
		int volumen = this.hoehe * this.breite * this.tiefe;
		return volumen;
	}
	
	public String getFarbe() {
		return this.farbe;
	}
}
