
public class Aufgabe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String s = "**"; 
		// Standardausgabe 
		System.out.printf( "%5s\n", s );
		
		String a = "*";
		// linksbŁndig mit Stellen 
		System.out.printf( "%-5s  %-5s\n", a, a);    
		
		String b = "*";
		// linksbŁndig mit Stellen 
		System.out.printf( "%-5s  %-5s\n", b, b);
		
		String c = "**"; 
		// Standardausgabe 
		System.out.printf( "%5s\n", c );
	}

}
