import java.util.Scanner; // Import der Klasse Scanner 
 
public class Abfrage  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie Ihren Namen ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    String name = myScanner.next();  
     
    System.out.print("Bitte geben Sie Ihr Alter ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    String alter = myScanner.next();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
     
    System.out.print("\nIhr Name und Alter: "); 
    System.out.print(name + " , " + alter + " Jahre alt ");
 
    myScanner.close(); 
     
  }    
}