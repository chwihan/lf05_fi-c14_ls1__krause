import java.util.Scanner;
public class Rom {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] zeichen = new String[7];
		zeichen[0] = "I = 1";
		zeichen[1] = "V = 5";
		zeichen[2] = "X = 10";
		zeichen[3] = "L = 50";
		zeichen[4] = "C = 100";
		zeichen[5] = "D = 500";
		zeichen[6] = "M = 1000";
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Welches R�mische Z�hlzeichen soll als Dezimalzahl ausgegeben werden? ");
		System.out.println("I (1)");
		System.out.println("V (2)");
		System.out.println("X (3)");
		System.out.println("L (4)");
		System.out.println("C (5)");
		System.out.println("D (6)");
		System.out.println("M (7)");
		
		int tab = tastatur.nextInt();
		
		switch (tab) {
		case 1:
			System.out.println(zeichen[0]);
			break;
		case 2:
			System.out.println(zeichen[1]);
			break;
		case 3:
			System.out.println(zeichen[2]);
			break;
		case 4:
			System.out.println(zeichen[3]);
			break;
		case 5:
			System.out.println(zeichen[4]);
			break;
		case 6:
			System.out.println(zeichen[5]);
			break;
		case 7:
			System.out.println(zeichen[6]);
			break;
		default:
			System.out.println("Fehler! Bitte einer Zahl von 1-7 eingeben!");
			break;
		}
	}

}
