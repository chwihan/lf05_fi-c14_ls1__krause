import java.util.ArrayList;

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private String schiffsname;
	private int androidenAnzahl;
	public ArrayList<Ladung> ladungsverzeichnis;
	
	
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	public void setPhotonentorpedoAnzahl(int anzahl) {
		this.photonentorpedoAnzahl = anzahl;
	}
	
	public int getEnergieversorungInProzent () {
		return this.energieversorgungInProzent;
	}
	public void setEnergieversorgungInProzent(int anzahl) {
		this.energieversorgungInProzent = anzahl;
	}
	
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	public void setSchildeInProzent(int anzahl) {
		this.schildeInProzent = anzahl;
	}
	
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	public void setHuelleInProzent(int anzahl) {
		this.huelleInProzent = anzahl;
	}
	
	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int anzahl) {
		this.lebenserhaltungssystemeInProzent = anzahl;
	}
	
	public String getSchiffsname() {
		return this.schiffsname;
	}
	public void setSchiffsname(String name) {
		this.schiffsname = name;
	}
	
	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	public void setAndroidenAnzahl(int anzahl) {
		this.androidenAnzahl = anzahl;
	}
	
 	public Raumschiff() {
		this.photonentorpedoAnzahl = 1;
		this.energieversorgungInProzent = 100;
		this.schildeInProzent = 100;
		this.huelleInProzent = 100;
		this.lebenserhaltungssystemeInProzent = 100;
		this.schiffsname = "Raumschiff";
		this.androidenAnzahl = 1;
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}
	
	public Raumschiff(int pA, int eP, int sP, int hP, int lP, String name, int aA) {
		this.photonentorpedoAnzahl = pA;
		this.energieversorgungInProzent = eP;
		this.schildeInProzent = sP;
		this.huelleInProzent = hP;
		this.lebenserhaltungssystemeInProzent = lP;
		this.schiffsname = name;
		this.androidenAnzahl = aA;
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}
	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);
	}
	
	public String toString() {
		return "Schiffsname: " + this.schiffsname + "\n" + "\n"+ " Photonentorpedo Anzahl: " + this.photonentorpedoAnzahl + "\n"+ " Energieversorgung in Prozent: "
	+ this.energieversorgungInProzent +"\n"+ " Schilde in Prozent: " + this.schildeInProzent + "\n" + " Huelle in Prozent: " + this.huelleInProzent +"\n"+ " Lebenserhaltungssysteme in Prozent:"
				+ this.lebenserhaltungssystemeInProzent +"\n"+ " Androiden Anzahl: " + this.androidenAnzahl + "\n "; 
	}
	
	public boolean photonentorpedo () {
		boolean torpedo = true;
		if (getPhotonentorpedoAnzahl() >= 1) {
			setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl()-1);
			System.out.println(getSchiffsname()+ ": Photonentorpedo Abgeschossen" + "\n" 
			+ "Anzahl Photonentorpedos: " + getPhotonentorpedoAnzahl() + "\n");
		}
		else {
			System.out.println(getSchiffsname()+ ": -=*Click*=-"
		+ "\n" + "Anzahl Photonentorpedos: " + getPhotonentorpedoAnzahl() + "\n");
			torpedo = false;
			
		}
		return torpedo;
	}	
	public boolean phaserkanone () {
		boolean energie = true;
		if (getEnergieversorungInProzent() >= 50) {
			setEnergieversorgungInProzent(getEnergieversorungInProzent()-50);
			System.out.println(getSchiffsname()+ ": Phaserkanone abgeschossen" + "\n" 
					+ "Energieversorgung in Prozent: " + getEnergieversorungInProzent());
		}
		else {
			System.out.println(getSchiffsname()+": -=*Click*=-"+ "\n" 
					+ "Energieversorgung in Prozent: " + getEnergieversorungInProzent());
			energie = false;
		}
		return energie;
	}
	

	
	public void treffer(boolean torpedo, boolean energie) {
		if (torpedo & energie) {
			// erstelle eine String Liste mit allen Schiffen -> such random ein Schiff aus
			String ship = new String[] {"IKS Hegh'ta", "IRW Khazara", "Ni'Var"}[(int)(Math.random()*3)];
			String ship1 = new String[] {"IKS Hegh'ta", "IRW Khazara", "Ni'Var"}[(int)(Math.random()*3)];
			System.out.println("\n" + ship + " und " + ship1 + " wurden getroffen!");
		}
		else if (torpedo){
			String ship = new String[] {"IKS Hegh'ta", "IRW Khazara", "Ni'Var"}[(int)(Math.random()*3)];
			String ship1 = new String[] {"IKS Hegh'ta", "IRW Khazara", "Ni'Var"}[(int)(Math.random()*3)];
			System.out.println("\n" + ship + " wurde getroffen!");
			System.out.println("\n" + ship1 + " wurde nicht getroffen!");
		}
		else if (energie) {
			String ship = new String[] {"IKS Hegh'ta", "IRW Khazara", "Ni'Var"}[(int)(Math.random()*3)];
			String ship1 = new String[] {"IKS Hegh'ta", "IRW Khazara", "Ni'Var"}[(int)(Math.random()*3)];
			System.out.println("\n" + ship + " wurde nicht getroffen!");
			System.out.println("\n" + ship1 + " wurde getroffen!");
		}
		else {
			String ship = new String[] {"IKS Hegh'ta", "IRW Khazara", "Ni'Var"}[(int)(Math.random()*3)];
			String ship1 = new String[] {"IKS Hegh'ta", "IRW Khazara", "Ni'Var"}[(int)(Math.random()*3)];
			System.out.println("\n" + ship + " und " + ship1 + "wurden nicht getroffen!");
		}
	}
	
	
}
